package com.example.akshatgupta.fragments;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.SymbolTable;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Time;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
public class MainActivity extends AppCompatActivity  {
    CaldroidListener listener;
    Date dat;
    Deadlines d;
    CaldroidFragment caldroidFragment;
    FeedbackForm feed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
    d= new Deadlines();
        feed= new FeedbackForm();

        setContentView(R.layout.activity_main);


        Dead q = new Dead(getIntent().getExtras().getString("username"),"nfejk");
        q.execute();
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
         caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        caldroidFragment.setArguments(args);

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar, caldroidFragment);
        t.commit();






    }
    public class Dead extends AsyncTask<String, Void, String> {
        OutputStream os = null;
        String url = "http://10.0.2.2:8014/firstapp/get_deadlines/";
        String url2 = "http://10.0.2.2:8014/firstapp/get_feed/";

        String a, b;

        public Dead(String c , String d) {
            a = c;
            b = d;
        }

        String f="";
        String f2="";

        @Override
        protected String doInBackground(String... params) {
            try {

                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("username", a));
                nameValuePairs.add(new BasicNameValuePair("password", b));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(httpPost);
                BufferedReader rd = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent()));
                String line = "";

                while ((line = rd.readLine()) != null) {
                    f = f + line;
                }
                System.out.println(f);
            }

            catch (Exception e) {
                e.printStackTrace();
            }

                JSONArray jon = null;
                try {
                    jon = new JSONArray(f);

                for(int i=0;i<jon.length();i++)
                {
                    JSONObject p = null;
                    p = jon.getJSONObject(i);
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date startDate = null;
                    try {
                        startDate = df.parse(p.getString("date"));
                        SimpleDateFormat tsimple = new SimpleDateFormat("hh:mm:ss");
                        Date d1 = null;
                        d1 = tsimple.parse(p.getString("time"));
                        Time t = new Time(d1.getTime()) ;
                        d.add(p.getString("name"),p.getString("description"),startDate,t,p.getString("course"));
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }


                }
            }
                catch (JSONException e) {
                    Log.e("JSON Parser", "Error parsing data " + e.toString());
                }



            try {

                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url2);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("username", a));
                nameValuePairs.add(new BasicNameValuePair("password", b));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(httpPost);
                BufferedReader rd = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent()));
                String line = "";

                while ((line = rd.readLine()) != null) {
                    f2 = f2 + line;
                }
            }

            catch (Exception e) {
                e.printStackTrace();
            }

            JSONArray jon2 = null;
            try {
                jon2 = new JSONArray(f2);
                for(int i=0;i<jon2.length();i++)
                {
                    JSONObject p2 = null;
                    p2 = jon2.getJSONObject(i);

                    System.out.println(f2);
                    try {

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        Date startDate = null;
                        startDate = df.parse(p2.getString("date"));
                        SimpleDateFormat tsimple = new SimpleDateFormat("hh:mm:ss");
                        Date d1 = null;
                        d1 = tsimple.parse(p2.getString("time"));
                        Time t = new Time(d1.getTime()) ;

                        feed.add(p2.getString("name"),p2.getString("course_name"),startDate,t);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }


                }
            }
            catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }

            return f;
        }

        protected void onPostExecute(String page) {



            listener = new CaldroidListener() {

                @Override
                public void onSelectDate(Date date, View view) {




                    FeedbackForm feeds = new FeedbackForm();
                    Deadlines a =new Deadlines();
                    String s="";
                    for(int i=0;i<feed.name.size();i++)
                    {
                        if(feed.edate.get(i)==date)
                        {
                            s=s+feed.name.get(i)+feed.Course.get(i);

                        }

                    }
                    final Context context = getApplicationContext();
                       final int duration = Toast.LENGTH_LONG;
                    Toast t = Toast.makeText(context, s, duration);
                    t.show();
//                    List<String> result =new ArrayList<String >();
//
//                    for (int i=0;i<a.course.size();i++)
//                    {
//
//                        Toast t = Toast.makeText(context, "Course Name:"+ a.course.get(i)+" Name: "+ a.name.get(i), duration);
//                        t.show();
//                    }
//                    for (int i=0;i<feeds.Course.size();i++)
//                    {
//                        result.add( "Course Name:"+ feeds.Course.get(i)+" Name: "+ feeds.name.get(i)  ) ;
//                        final Context context = getApplicationContext();
//                        final int duration = Toast.LENGTH_LONG;
//                        Toast t = Toast.makeText(context, "Course Name:"+ feeds.Course.get(i)+" Name: "+ feeds.name.get(i), duration);
//                        t.show();
//                    }





                }

                @Override
                public void onChangeMonth(int month, int year) {
                    String text = "month: " + month + " year: " + year;

                }

                @Override
                public void onLongClickDate(Date date, View view) {
                    final Context context = getApplicationContext();
                    final int duration = Toast.LENGTH_LONG;
                    Toast t = Toast.makeText(context, "Holding longer Change it :P", duration);
                    t.show();

                }

                @Override
                public void onCaldroidViewCreated() {


                }

            };

            caldroidFragment.setCaldroidListener(listener);
            System.out.println(feed.name.size());
            for (int i = 0, len = d.name.size(); i < len; i++) {
                Date myd = d.edate.get(i);
                ColorDrawable green = new ColorDrawable(Color.GREEN);
                caldroidFragment.setBackgroundDrawableForDate(green, myd);
                caldroidFragment.refreshView();

            }
            for (int i = 0, len = feed.name.size(); i < len; i++) {

                Date myd = feed.edate.get(i);

                //color myd!!!

                ColorDrawable blue = new ColorDrawable(Color.BLUE);
                caldroidFragment.setBackgroundDrawableForDate(blue, myd);
                caldroidFragment.refreshView();

            }


        }
        }
    }

