package com.example.akshatgupta.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class Main4Activity extends AppCompatActivity {

    EditText username;
    EditText password;
    Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        button = (Button) findViewById(R.id.button2);
        int cor = 0;
        SharedPreferences setting = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//
//        if(setting.getString("username", null)!=null){
//
//            verify q = new verify(setting.getString("username",null),setting.getString("password",null) );
//                q.execute();
//
//
//
//            Intent intent = new Intent(Main4Activity.this, MainActivity.class);
//            intent.putExtra("username",setting.getString("username",setting.getString("password",null)));
//            startActivity(intent);
//        }
        button.setOnClickListener(new Button.OnClickListener() {

            public void onClick(View v) {

                String c = username.getText().toString();
                String d = password.getText().toString();
                verify q = new verify(c, d);
                q.execute();

            }
        });
    }
//    @Override    //ADD later
//    protected  void onResume()
//    {
//        super.onResume();
//        SharedPreferences setting = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//
//        if(setting.getString("username", null)!=null){
//            Intent intent = new Intent(Main4Activity.this, Main3Activity.class);
//            startActivity(intent);
//            System.out.println("Here");
//        }
//    }

    public class verify extends AsyncTask<String, Void, String> {
        OutputStream os = null;
        String url = "http://10.0.2.2:8014/firstapp/abc/";


        String a, b;

        public verify(String c, String d) {
            a = c;
            b = d;
            System.out.println(a);
            System.out.println(b);
        }

        String f="";

        @Override
        protected String doInBackground(String... params) {
            try {
//                    int temp;
//
//                    URL ur = new URL(url);
//                    String urlparams = "username=" + a+"&password="+b;
//                    HttpURLConnection httpURLConnection = (HttpURLConnection) ur.openConnection();
//                    OutputStream os = httpURLConnection.getOutputStream();
//                    os.write(urlparams.getBytes());
//                    os.flush();
//                    os.close();
//                    InputStream is = httpURLConnection.getInputStream();
//                    while ((temp=is.read())!=-1)
//                    {
//                        data=data+(char)temp;
//                    }

                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("username", a));
                nameValuePairs.add(new BasicNameValuePair("password", b));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(httpPost);
                BufferedReader rd = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent()));
                String line = "";

                while ((line = rd.readLine()) != null) {
                    f = f + line;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return f;
        }

        protected void onPostExecute(String page) {
            String red = "1";
            System.out.print(f);

            if (f.equals(red)) {
                System.out.print(f);

                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("username", String.valueOf(username));
                editor.putString("password", String.valueOf(password));
                editor.commit();

                Intent intent = new Intent(Main4Activity.this, MainActivity.class);
                intent.putExtra("username",a);
                startActivity(intent);

            } else {
                final Context context = getApplicationContext();
                final int duration = Toast.LENGTH_LONG;
                Toast t = Toast.makeText(context, "Wrong Username or password", duration);
                t.show();
            }

        }
    }
}

