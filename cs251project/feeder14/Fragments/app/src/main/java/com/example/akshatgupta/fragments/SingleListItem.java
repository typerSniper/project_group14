package com.example.akshatgupta.fragments;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class SingleListItem extends Activity {
@Override
public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_single_list_item);
    TextView date = (TextView) findViewById(R.id.date);
    TextView time= (TextView) findViewById(R.id.time);
    TextView name = (TextView) findViewById(R.id.name);
    TextView description = (TextView) findViewById(R.id.description);

    TextView coursename = (TextView) findViewById(R.id.coursename);

        Intent i = getIntent();
        // getting attached intent data
      name.setText(i.getStringExtra("name"));
    description.setText(i.getStringExtra("description"));
    time.setText(i.getStringExtra("time"));
    date.setText(i.getStringExtra("date"));
    coursename.setText(i.getStringExtra("course"));



        // displaying selected product name

        }
}