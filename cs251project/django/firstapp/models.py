import datetime
from django.db import models
from django.utils import timezone
from django.forms import formset_factory


class courses(models.Model):
    course_name= models.CharField(max_length=10)
    def __str__(self):
      return self.course_name

class Deadline(models.Model):
    name=models.CharField(max_length=100)
    time=models.TimeField()
    date = models.DateField(blank=True, null=True)
    description=models.TextField(max_length=1000)
    course = models.CharField(max_length=10)
    def __str__(self):
        return self.name

class FeedbackForm(models.Model):
    name = models.CharField(max_length=100)
    course_name = models.CharField(max_length=100)
    time=models.TimeField(blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    def __str__(self):
        return self.name

class Questio(models.Model):
    question_text = models.CharField(max_length=200)
    forname = models.CharField(max_length=100)
    question_id = models.IntegerField() 
    course_name = models.CharField(max_length=10)
    def __str__(self):
        return self.question_text

class Rating(models.Model):
    question = models.OneToOneField(Questio,on_delete=models.CASCADE, blank=True, null=True)
    countone = models.IntegerField(default=0)
    counttwo = models.IntegerField(default=0)
    countthree = models.IntegerField(default=0)
    countfour = models.IntegerField(default=0)
    countfive = models.IntegerField(default=0)
    Optionone = models.CharField(max_length=50,default="Very Poor")
    Optiontwo = models.CharField(max_length=50,default="Poor")
    Optionthree = models.CharField(max_length=50,default="Average")
    Optionfour = models.CharField(max_length=50,default="Good")
    Optionfive = models.CharField(max_length=50,default="Excellent")


class Description(models.Model):
    text = models.CharField(max_length=300)
    question = models.OneToOneField(Questio,on_delete=models.CASCADE, blank=True, null=True)

class Student(models.Model):
    name = models.CharField(max_length=100)
    roll_number  = models.CharField(max_length=50)
    courses_undertaken = models.ManyToManyField(courses)
    password = models.CharField(max_length=50, blank=True, null=True)
    email  = models.EmailField(blank=True, null=True)
    def __str__(self):
      return self.name
   