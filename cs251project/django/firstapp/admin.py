from django.contrib import admin
from .models import *
admin.site.register(Questio)
admin.site.register(courses)
admin.site.register(Student)
admin.site.register(Deadline)
admin.site.register(FeedbackForm)

# Register your models here.
