# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-01 13:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0006_remove_questio_course_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='questio',
            name='course_name',
            field=models.CharField(default='cse', max_length=10),
            preserve_default=False,
        ),
    ]
