# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-04 17:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0009_auto_20161104_1703'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questio',
            name='forname',
            field=models.CharField(default='none', max_length=100),
            preserve_default=False,
        ),
    ]
