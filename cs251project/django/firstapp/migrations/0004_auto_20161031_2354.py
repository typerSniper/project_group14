# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-31 23:54
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0003_deadline_description'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeedbackForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='deadline',
            name='course',
            field=models.ForeignKey(default='101', on_delete=django.db.models.deletion.CASCADE, to='firstapp.courses'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='questio',
            name='course_name',
            field=models.CharField(default='101', max_length=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='deadline',
            name='description',
            field=models.TextField(max_length=1000),
        ),
    ]
