from django.conf.urls import *
from django.contrib import admin

from . import views

urlpatterns = [
    url(r'^register/', views.register),
    url(r'^login/', views.my_login),
    url(r'^feed/', views.myview),
    url(r'^delete/', views.deletebyid),
    url(r'^logout/', views.my_logout),
    url(r'^add_deadline/', views.add_deadline),
    url(r'^Course_reg/', views.add_course),
    url(r'^add_students/', views.add_students),
    url(r'^see_courses/', views.see_courses),
    url(r'^prep/', views.prep),
    url(r'^abc/', views.studentCheck.as_view()),
    url(r'^get_deadlines/', views.deadList.as_view()),
    url(r'^get_feed/', views.feedList.as_view()),
    url(r'^get_question/', views.questionsList.as_view()),




]
