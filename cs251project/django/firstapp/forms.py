from django.contrib.auth.models import User
from django import forms
from django.db import models
from django.forms import formset_factory
from django.forms.formsets import BaseFormSet

class RegisterForm(forms.Form):
    first_name=forms.CharField(required=True)
    last_name=forms.CharField(required=False)
    username=forms.CharField(required=True)
    email=forms.EmailField(required=False)
    password=forms.CharField(widget=forms.PasswordInput(), required= True)
    re_enter_password=forms.CharField(widget=forms.PasswordInput(), required=True)

class LoginForm(forms.Form):
    username=forms.CharField(required=True)
    password=forms.CharField(widget=forms.PasswordInput(), required=True)

class feedbackform(forms.Form):
    question_text = forms.CharField(required=True)
    form_name = forms.CharField(required=True)
    Course_name=forms.CharField(required=True)
    time = forms.TimeField(label="Time   ", required=True)
    date = forms.DateField(label="Date", required=True)
    question_id=forms.IntegerField()

class course_form(forms.Form):
    cname = forms.CharField(required=True)
    fname = forms.CharField(required=False)

class deletionform(forms.Form):
    form_name = forms.CharField(label="",required=True)
    Course_name=forms.CharField(label="",required=True)
    del_id=forms.IntegerField(label="Specify Id To be Deleted")
    curr_id = forms.IntegerField(label="")

class DeadlineForm(forms.Form):
    Course_name = forms.CharField(label="course",required=True)
    name = forms.CharField(label="Assignment ", required=True)
    time = forms.TimeField(label="Time   ", required=True)
    date = forms.DateField(label="Date", required=True)
    description=forms.CharField(widget= forms.Textarea, label="Description",required=True)


class CourseForm(forms.Form):
    name = forms.CharField(required=True)

class StudentForm(forms.Form):
    file = forms.FileField(required=True)
    course_name = forms.CharField(required=True)

