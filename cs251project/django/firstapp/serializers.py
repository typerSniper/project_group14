from rest_framework import serializers
from .models import * 

class question_serializer(serializers.ModelSerializer):
	class Meta:
		model = Questio
		fields= '__all__'
	

class course_serializer(serializers.ModelSerializer):
	class Meta:
		model = courses
		fields='__all__'


class deadline_serializer(serializers.ModelSerializer):
	class Meta:	
		model = Deadline
		fields='__all__'

class feedback_form_serializer(serializers.ModelSerializer):
	
	class Meta:
		model = FeedbackForm
		fields='__all__'
