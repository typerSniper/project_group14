from .forms import *
from .models import *
from django.contrib.auth import login, logout, authenticate
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponse
import csv
import os
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from .serializers import *
from django.db.models import Q
def my_login(request):
    if request.method=="POST":
        form = LoginForm(request.POST)
        if form.is_valid() :
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
           
            if(password=="facebook_django_12"):
               url="https://graph.facebook.com/debug_token?input_token="+username+"&access_token=1771269733140485|2cc98eedf21b7ef09a551a88b87524c5"
               f = urllib.request.urlopen(url)
               return HttpResponse(f)
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                all_courses=courses.objects.all();
                if user.is_superuser:
                    return render(request, 'instr_home.html', {'user': user, 'allC':all_courses, 'superU':True})   
                else:
                    return render(request, 'instr_home.html', {'user': user, 'allC':all_courses, 'superU':False})      
            else:
                return render(request, 'error.html', {'errorM':"Wrong Username or Password", 'link':"http://localhost:8014/firstapp/login", 'butt':"Try Again"})
    
    else:
        if request.user.is_authenticated:  ## Does not allow two users to log in the same session (whatever that means!)
            all_courses=courses.objects.all;
            if request.user.is_superuser:
                return render(request, 'instr_home.html', {'user': request.user, 'allC':all_courses, 'superU':True})   
            else:
                return render(request, 'instr_home.html', {'user': request.user, 'allC':all_courses, 'superU':False})
        else:
            form1=LoginForm();
            return render(request, 'login.html', {'form':form1})
#no

def register(request):
    if not request.user.is_authenticated:
        if request.method == "POST":
            form = RegisterForm(request.POST)
            if form.is_valid():
                if User.objects.filter(username=form.cleaned_data['username']).exists():
                    return render(request, 'error.html', {'errorM':"Username already taken", 'link':"http://localhost:8014/firstapp/register", 'butt':"Try Again"})
                else:
                    if form.cleaned_data['password'] == form.cleaned_data['re_enter_password']:
                        new_user = User.objects.create_user(username=form.cleaned_data['username'], password=form.cleaned_data['password'], first_name=form.cleaned_data['first_name'], last_name=form.cleaned_data['last_name'], email=form.cleaned_data['email'])
                        return render(request, 'error.html', {'errorM':"Successfully Registered", 'link':"http://localhost:8014/firstapp/login", 'butt':"Sign In"})
                    else:
                        return render(request, 'error.html', {'errorM':"Passwords don't match", 'link':"http://localhost:8014/firstapp/register", 'butt':"Try Again"})
            else:
                return render(request, 'error.html', {'errorM':"Something Went Wrong!", 'link':"http://localhost:8014/firstapp/register", 'butt':"Try Again"})  
        else:
            form = RegisterForm() 
            return render(request, 'register.html', {'form': form}) 
    else:
        return render(request, 'error.html', {'errorM':"You are already Logged in!", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
#no

def myview(request):
    if request.user.is_authenticated:
        if not request.user.is_superuser:
            if request.method == 'POST':
                form = feedbackform(request.POST)
                if form.is_valid():
                    fname=form.cleaned_data['form_name']
                    qid = form.cleaned_data['question_id']+1
                    cname = form.cleaned_data['Course_name']
                    courseObj= courses.objects.filter(course_name=cname)
                    if courseObj.count()==0:
                        return render(request, 'error.html', {'errorM':"Course Does not Exist", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
                    question_till= Questio.objects.filter(forname=fname).filter(course_name=cname)
                    if question_till.count() != 0:
                        return render(request, 'error.html', {'errorM':"Same form already exists", 'link':"http://localhost:8014/firstapp/feed", 'butt':"Try Again"})
                    else:
                        q = Questio.objects.create(question_id=qid-1,question_text=form.cleaned_data['question_text'], course_name=cname, forname=fname)
    
                        tform = feedbackform({'form_name':form.cleaned_data['form_name'],'Course_name':form.cleaned_data['Course_name'],'question_id':qid, 'question_text':"Add first question", 'time':form.cleaned_data['time'], 'date':form.cleaned_data['date']})
                        question_till= Questio.objects.filter(forname=fname).filter(course_name=cname)
                        ttform= deletionform({'form_name':fname,'Course_name':cname,'curr_id':qid,'del_id':0})
                        return render(request,"myview.html",{'formf':tform, 'formd':ttform ,'ques':question_till})
                else:
                    return render(request, 'error.html', {'errorM':"Something Went Wrong!", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
            else:
                form = feedbackform({'question_id':1, 'form_name':"Add title", 'Course_name':"Add Course name", 'question_text':"Add first question"})
                return render(request, "myview.html", { 'formf': form })
        else:
            return render(request, 'error.html', {'errorM':"Access Denied", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})

    else:
        return render(request, 'error.html', {'errorM':"Log-In First ;)", 'link':"http://localhost:8014/firstapp/login", 'butt':"This Way!"})
#done

def deletebyid(request):
    if request.method =='POST':
        form = deletionform(request.POST)
        if form.is_valid():
            fname=form.cleaned_data['form_name']
            qid = form.cleaned_data['del_id']
            cname = form.cleaned_data['Course_name']
            cid = form.cleaned_data['curr_id']
            Questio.objects.filter(forname=fname).filter(course_name=cname).filter(question_id=qid).delete()
            question_till= Questio.objects.filter(forname=fname).filter(course_name=cname)
            tform = feedbackform({'form_name':fname,'Course_name':cname,'question_id':cid, 'question_text':"Add  question"})
            ttform= deletionform({'form_name':fname,'Course_name':cname,'curr_id':cid,'del_id':0})
            return render(request,"myview.html",{'formd': ttform, 'formf':tform,'ques':question_till})

    else:
        return render(request, 'error.html', {'errorM':"Something Went Wrong!", 'link':"http://localhost:8014/firstapp/feed", 'butt':"Try Again"})
#done

def my_logout(request):
    logout(request)
    return render(request, 'error.html', {'errorM':"Logged Out Successfully", 'link':"http://localhost:8014/firstapp/login", 'butt':"Log In Again"})
#no

def add_deadline(request):
    if request.user.is_authenticated:
        if not request.user.is_superuser :
            if request.method =='POST':
                form = DeadlineForm(request.POST)
                if form.is_valid():
                    cname = form.cleaned_data['Course_name']
                    dname=form.cleaned_data['name']
                    dtime=form.cleaned_data['time']
                    ddate=form.cleaned_data['date']
                    ddes=form.cleaned_data['description']
                    Deadline.objects.create(name=dname, time=dtime, description=ddes, date=ddate,course=cname)
                    return render(request, 'error.html', {'errorM':"Deadline Added Successfully", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
                else:
                    return render(request, 'error.html', {'errorM':"Something Went Wrong!", 'link':"http://localhost:8014/firstapp/add_deadline", 'butt':"Try Again"})
            else:
                form = DeadlineForm() 
                return render(request, 'deadline.html', {'form': form})
        else:
            return render(request, 'error.html', {'errorM':"Access Denied", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
    else:
        return render(request, 'error.html', {'errorM':"Log-In First ;)", 'link':"http://localhost:8014/firstapp/login", 'butt':"This Way!"})
#modified

def add_course(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            if request.method =='POST':
                form = CourseForm(request.POST)
                if form.is_valid():
                    cname=form.cleaned_data['name']
                    courseCheck = courses.objects.filter(course_name=cname)
                    if courseCheck.count() == 0:
                        c = courses.objects.create(course_name=cname)
                        x = "Mid Sem"
                        y = "End Sem"
                        FeedbackForm.objects.create(name= x,course_name=cname)
                        FeedbackForm.objects.create(name= y,course_name=cname)

                        q = Questio.objects.create(question_id=1,forname=x,question_text="Rate the difficulty of questions", course_name=cname)
                       
                        q = Questio.objects.create(question_id=2,forname=x,question_text="Rate the ", course_name=cname)
                       
                        q = Questio.objects.create(question_id=1,forname=y,question_text="Rate the difficulty of questions", course_name=cname)
                      
                        q = Questio.objects.create(question_id=2,forname=y,question_text="Rate the ", course_name=cname)
                        
                        form1 = StudentForm()
                        return render(request, 'add_students.html', {'course_name':cname, 'form': form1})
                    else:
                        return render(request, 'error.html', {'errorM':"Course Already Exists!", 'link':"http://localhost:8014/firstapp/Course_reg/", 'butt':"Try Again"})
                else:
                    return render(request, 'error.html', {'errorM':"Something Went Wrong!", 'link':"http://localhost:8014/firstapp/Course_reg/", 'butt':"Try Again"})
            else:
                form = CourseForm() 
                return render(request, 'add_course.html', {'form': form})
        else:
            return render(request, 'error.html', {'errorM':"Access Denied", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
    else:
        return render(request, 'error.html', {'errorM':"Log-In First ;)", 'link':"http://localhost:8014/firstapp/login", 'butt':"This Way!"})
#done

def add_students(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            if request.method =='POST':
                form = StudentForm(request.POST, request.FILES)
                if form.is_valid():
                    courseN = form.cleaned_data['course_name']
                    r=courses.objects.filter(course_name=courseN)
                    if(r.count()!=0):
                        f = request.FILES['file']
                        with open("cor.csv", 'wb+') as destination:
                            for chunk in f.chunks():
                                    destination.write(chunk)
                        add_csv_students(courseN)
                        os.remove("cor.csv")
                        return render(request, 'error.html', {'errorM':"Students Successfully Added!", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})

                    else:
                        return render(request, 'error.html', {'errorM':"404 Course Doesn't Exist", 'link':"http://localhost:8014/firstapp/Course_reg", 'butt':"Add This"})     
                else:
                    return render(request, 'error.html', {'errorM':"Something Went Wrong!", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})     
            else:
                form1 = StudentForm()
                return render(request, 'add_students.html', {'form': form1})
        else:
            return render(request, 'error.html', {'errorM':"Access Denied", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
    else:
        return render(request, 'error.html', {'errorM':"Log-In First ;)", 'link':"http://localhost:8014/firstapp/login", 'butt':"This Way!"})
#done

def add_csv_students(cname):
    with open('cor.csv') as g:
        reader = csv.reader(g,delimiter = ',')
        rows = list(reader)
    for row in rows:
        sname =row[0];
        sroll = row[1];
        semail=row[2];
        spass=row[3];
        adds(sname, cname, sroll, semail, spass)
#done

def adds(sname,cname,sroll, semail, spass):
    r = courses.objects.filter(course_name=cname)
    v = Student.objects.filter(roll_number=sroll)
    if(v.count()!=0): 
        for stu in v:
            for course in r:
                stu.courses_undertaken.add(course)
    else:
        for course in r:
            Sudent=Student.objects.create(name=sname,roll_number=sroll, password=spass, email=semail)
            Sudent.courses_undertaken.add(course)
#done

def prep(request):
    if request.user.is_authenticated:
        if not request.user.is_superuser:
            if request.method=="POST":
                form = feedbackform(request.POST);
                if form.is_valid():
                    fname = form.cleaned_data['form_name'];
                    cname = form.cleaned_data['Course_name'];
                    time = form.cleaned_data['time']
                    date=form.cleaned_data['date']
                    
                    feed =FeedbackForm.objects.create(name= fname,course_name=cname, date=date, time=time)
                   
                    quesl = Questio.objects.filter(forname=fname).filter(course_name=cname)
                    return render(request, 'courses.html', {'quesl':quesl})
                else:
                    return render(request, 'error.html', {'errorM':"Something Went Wrong!", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
            else:
                return render(request, 'error.html', {'errorM':"Access Denied", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
        else:
            return render(request, 'error.html', {'errorM':"Access Denied", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
    else:
        return render(request, 'error.html', {'errorM':"Log-In First ;)", 'link':"http://localhost:8014/firstapp/login", 'butt':"This Way!"})
#done

def see_courses(request):
    if request.user.is_authenticated:
        if not request.user.is_superuser:
            if request.method=="POST":
                form = course_form(request.POST)
                if form.is_valid():
                    cname = form.cleaned_data['cname']
                    formsl = FeedbackForm.objects.filter(course_name=cname)
                    if form.cleaned_data['fname'] =="":
                        if formsl.count() ==0:
                            return render(request, 'error.html', {'errorM':"No Form for the selected course", 'link':"http://localhost:8014/firstapp/feed", 'butt':"Add a Form"})
                        else:
                            return render(request, 'courses.html', {'form': form, 'formsl':formsl})
                    else:
                        formn = form.cleaned_data['fname']
                        quesl = Questio.objects.filter(forname=formn).filter(course_name=cname)
                        return render(request, 'courses.html', {'form':form, 'quesl':quesl, 'formsl':formsl})
                else:
                    return render(request, 'error.html', {'errorM':"Something Went Wrong!", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
            else:
                form = course_form();
                return render(request, 'courses.html', {'form':form})
        else:
            return render(request, 'error.html', {'errorM':"Access Denied", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
    else:
        return render(request, 'error.html', {'errorM':"Log-In First ;)", 'link':"http://localhost:8014/firstapp/login", 'butt':"This Way!"})
#maybe done

class studentCheck(APIView):
    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']

        s = Student.objects.filter(name=username).filter(roll_number=password)
        if s.count()!=0:
            return Response(1)
        else:
            return Response(0)


class questionsList(APIView):
    def get(self, request): 

        serializer = question_serializer(courses, many=True)
        return Response(serializer.data,content_type="application/json")

    def post(self, request):
        username = request.POST['username']
        ji = Student.objects.filter(name=username)
        for p in ji:
            clist = p.courses_undertaken.all()
        a=[]
        for l in clist:
            p = list(Questio.objects.filter(course_name=l.course_name))
            a=a+p
        r=[]
        for j in a:
            r = r+[j.pk]

        d = Questio.objects.filter(pk__in=r)
        serializer= question_serializer(d, many=True)
        return Response(serializer.data, content_type='application/json')

class deadList(APIView):
    # def get(self, request):
        
    #     username = "Jatin"
    #     clist  =Student.objects.filter(name=username)[:1][0].courses_undertaken.all() 
    #     t = Deadline.objects.filter(course=clist[0])
    #     a=[]
    #     for c in clist:
    #         p = list(Deadline.objects.filter(course=c))
    #         a=a+p
    #     r=[]
    #     for j in a:
    #         r = r+[j.pk]
    #     d = Deadline.objects.filter(pk__in=r)
    #     serializer = deadline_serializer(d, many=True)
    #     return HttpResponse(serializer.data, content_type='application/json')

    #     # for c in clist:
           
    #     # for s in dList:
    #     #     q=s.FeedbackForm_set.all()
    #     #     if(q.count()==0):
    #     #         allfeeded=allfeeded+[q.pk]
    #     # dList = Deadline.objects.filter(pk__in=allfeeded)
    #     # serializer = deadline_serializer(dList, many=True)
    #     # return Response(serializer.data, content_type='application/json')
    #     # a = "{"+'"deadlines"'+":"serializer.data()+"}"
    #     # b= "{"+'"questions"'+":"+serializer1.data()+"}"
    #     # c = "["+a+","+b+"]"

    def post(self, request):
        username = request.POST['username']
        print(username)

        ji = Student.objects.filter(name=username)
        for p in ji:
            clist = p.courses_undertaken.all()
        a=[]
        for c in clist:
            p = list(Deadline.objects.filter(course=c.course_name))
            a=a+p
        r=[]
        for j in a:
            r = r+[j.pk]
        d = Deadline.objects.filter(pk__in=r)
        serializer = deadline_serializer(d, many=True)
        return Response(serializer.data, content_type='application/json')


class feedList(APIView):
    # def get(self, request):
    #     username="Akshat"

    #     clist  =Student.objects.filter(name=username)[:1][0].courses_undertaken.all()
    #     a=[]
    #     for l in clist:
    #         p = list(FeedbackForm.objects.filter(course_name=l.course_name))
    #         a=a+p
    #     r=[]
    #     for j in a:
    #         r = r+[j.pk]

    #     d = FeedbackForm.objects.filter(pk__in=r)
    #     serializer= feedback_form_serializer(d, many=True)
    #     return HttpResponse(serializer.data, content_type='application/json')



    def post(self, request):
        username = request.POST['username']
        print(username)

        ji = Student.objects.filter(name=username)
        for p in ji:
            clist = p.courses_undertaken.all()
        a=[]
        for l in clist:
            p = list(FeedbackForm.objects.filter(course_name=l.course_name))
            a=a+p
        r=[]
        for j in a:
            r = r+[j.pk]

        d = FeedbackForm.objects.filter(pk__in=r)
        serializer= feedback_form_serializer(d, many=True)
        return Response(serializer.data, content_type='application/json')


