Working As of 2 November:
Only the web app, which is expected to act as a backend for android app, has been documented here.

Git commit id (final commit id for lab10): 9a07b4d41852f92dbadacd58958e1d1e9defab03

1) There is an admin login page. This is a special instructor who has an exclusive right to Add courses/Students to the framework, the credentials of whom     are given at the end.
2) Link to login Page: http://localhost:8014/firstapp/login/
3) On signing in, the admin lands up at his homepage where he can view the currently running courses and number of students enrolled in each of them.
4) He can navigate over to two pages where he can add students/courses.
5) All these internal urls are inaccessible without login.
6) Each time a course is added, Two feedback forms named Mid Sem and End Sem and two deadlines will be created automatically.These as stated won�t 	be visible to the created admin but only the instructors. Since the Instructor page is under development, you will have to check their creation at the 	default admin page.
7) For Enrolment of students the following restrictions are in place:
	a) The enrolment has to be done by submitting a �.csv� file with exactly two columns , Name and Roll Number
	b) It has to be done one course at a time.
	c)  If the course to which addition is requested doesn�t exist, you will be redirected to  the add course section.
8) The course count in the default admin is nothing, It was for our usage. In the developed admin it is displayed correctly
Admin-Username : jatin
Password : feeder14