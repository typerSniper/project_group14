# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-02 14:29
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0003_deadline'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deadline',
            name='time',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now),
        ),
    ]
