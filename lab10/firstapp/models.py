from django.db import models
from datetime import datetime    
class courses(models.Model):
    course_name= models.CharField(max_length=10)
    count = models.IntegerField(default=0)
    def __str__(self):
      return self.course_name

class Questio(models.Model):
    question_text = models.CharField(max_length=200)
    forname = models.CharField(max_length=100)
    question_id = models.IntegerField() 
    course_name = models.CharField(max_length=10)
    def __str__(self):
        return self.question_text

class Student(models.Model):
    name = models.CharField(max_length=100)
    roll_number  = models.CharField(max_length=50)
    courses_undertaken = models.ManyToManyField(courses)
    def __str__(self):
      return self.name

class Deadline(models.Model):
    name=models.CharField(max_length=100)
    time=models.DateTimeField(default=datetime.now,blank=True)
    description=models.TextField(max_length=1000)
    def __str__(self):
        return self.name

class FeedbackForm(models.Model):
    name = models.CharField(max_length=100)
    course_name = models.CharField(max_length=100)   
    def __str__(self):
        return self.name
# Create your models here.
