from django.conf.urls import *
from django.contrib import admin
from . import views
urlpatterns = [  
  url(r'^login/', views.my_login),
  url(r'^logout/', views.my_logout),
  url(r'^Course_reg/', views.add_course),
  url(r'^add_students/', views.add_students),

]