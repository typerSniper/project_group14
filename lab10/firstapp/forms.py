from django.contrib.auth.models import User
from django import forms



class LoginForm(forms.Form):
    username=forms.CharField(required=True)
    password=forms.CharField(widget=forms.PasswordInput())

class CourseForm(forms.Form):
    name = forms.CharField(required=True)

class StudentForm(forms.Form):
    file = forms.FileField(required=True)
    course_name = forms.CharField(required=True)





