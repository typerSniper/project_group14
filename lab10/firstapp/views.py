from .forms import *
from .models import *
from django.contrib.auth import login, logout, authenticate
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from django.http import HttpResponse
import csv
import os
from datetime import datetime   

def my_login(request):
    if request.method=="POST":
        form = LoginForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            all_courses=courses.objects.all();
            if user.is_superuser:
                return render(request, 'instr_home.html', {'user': user, 'allC':all_courses, 'superU':True})   
            else:
                return render(request, 'instr_home.html', {'user': user, 'allC':all_courses, 'superU':False})      
        else:
            return render(request, 'error.html', {'errorM':"Wrong Username or Password", 'link':"http://localhost:8014/firstapp/login", 'butt':"Try Again"})

    else:
        if request.user.is_authenticated:  ## Does not allow two users to log in the same session (whatever that means!)
            all_courses=courses.objects.all;
            if request.user.is_superuser:
                return render(request, 'instr_home.html', {'user': request.user, 'allC':all_courses, 'superU':True})   
            else:
                return render(request, 'instr_home.html', {'user': request.user, 'allC':all_courses, 'superU':False})
        else:
            form1=LoginForm();
            return render(request, 'login.html', {'form':form1})

def my_logout(request):
    logout(request)
    return render(request, 'error.html', {'errorM':"Logged Out Successfully", 'link':"http://localhost:8014/firstapp/login", 'butt':"Log In Again"})

def add_course(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            if request.method =='POST':
                form = CourseForm(request.POST)
                if form.is_valid():
                    cname=form.cleaned_data['name']
                    courseCheck = courses.objects.filter(course_name=cname)
                    if courseCheck.count() == 0:
                        courses.objects.create(course_name=cname)
                        x = "Mid Sem"
                        y = "End Sem"
                        Questio.objects.create(question_id=1,forname=x,question_text="Rate the difficulty of questions", course_name=cname)
                        Questio.objects.create(question_id=2,forname=x,question_text="Rate the ", course_name=cname)
                        Questio.objects.create(question_id=1,forname=y,question_text="Rate the difficulty of questions", course_name=cname)
                        Questio.objects.create(question_id=2,forname=y,question_text="Rate the ", course_name=cname)
                        FeedbackForm.objects.create(name= x,course_name=cname)
                        FeedbackForm.objects.create(name= y,course_name=cname)
                        Deadline.objects.create(name=x, description="A new deadline")
                        Deadline.objects.create(name=y, description="A new deadline")
                        form1 = StudentForm()
                        return render(request, 'add_students.html', {'course_name':cname, 'form': form1})
                    else:
                        return render(request, 'error.html', {'errorM':"Course Already Exists!", 'link':"http://localhost:8014/firstapp/Course_reg/", 'butt':"Try Again"})
                else:
                    return render(request, 'error.html', {'errorM':"Something Went Wrong!", 'link':"http://localhost:8014/firstapp/Course_reg/", 'butt':"Try Again"})
            else:
                form = CourseForm() 
                return render(request, 'add_course.html', {'form': form})
        else:
            return render(request, 'error.html', {'errorM':"Access Denied", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
    else:
        return render(request, 'error.html', {'errorM':"Log-In First ;)", 'link':"http://localhost:8014/firstapp/login", 'butt':"This Way!"})

def add_students(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            if request.method =='POST':
                form = StudentForm(request.POST, request.FILES)
                if form.is_valid():
                    courseN = form.cleaned_data['course_name']
                    r=courses.objects.filter(course_name=courseN)
                    if(r.count()!=0):
                        f = request.FILES['file']
                        with open("cor.csv", 'wb+') as destination:
                            for chunk in f.chunks():
                                    destination.write(chunk)
                        add_csv_students(courseN)
                        os.remove("cor.csv")
                        return render(request, 'error.html', {'errorM':"Students Successfully Added!", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})

                    else:
                        return render(request, 'error.html', {'errorM':"404 Course Doesn't Exist", 'link':"http://localhost:8014/firstapp/Course_reg", 'butt':"Add This"})     
                else:
                    return render(request, 'error.html', {'errorM':"Something Went Wrong!", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})     
            else:
                form1 = StudentForm()
                return render(request, 'add_students.html', {'form': form1})
        else:
            return render(request, 'error.html', {'errorM':"Access Denied", 'link':"http://localhost:8014/firstapp/login", 'butt':"Home"})
    else:
        return render(request, 'error.html', {'errorM':"Log-In First ;)", 'link':"http://localhost:8014/firstapp/login", 'butt':"This Way!"})
        
def add_csv_students(cname):
    with open('cor.csv') as g:
        reader = csv.reader(g,delimiter = ',')
        rows = list(reader)
    for row in rows:
        sname =row[0];
        sroll = row[1];
        adds(sname, cname, sroll)

def adds(sname,cname,sroll):
    r = courses.objects.filter(course_name=cname)
    v = Student.objects.filter(name=sname).filter(roll_number=sroll)
    if(v.count()!=0): 
        for stu in v:
            for course in r:
                stu.courses_undertaken.add(course)
    else:
        for course in r:
            Sudent=Student.objects.create(name=sname,roll_number=sroll)
            Sudent.courses_undertaken.add(course)
