from django.contrib import admin
from .models import *

admin.site.register(Questio)
admin.site.register(courses)
admin.site.register(Student)
admin.site.register(FeedbackForm)
admin.site.register(Deadline)


# Register your models here.
