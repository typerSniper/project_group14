package com.example.jatin.loginf;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    EditText username;
    EditText password;
    Button button;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        button = (Button) findViewById(R.id.email_sign_in_button);
        int cor = 0;

        button.setOnClickListener(new Button.OnClickListener() {

            public void onClick(View v) {
//                try {
//                    URL url = new URL("http://10.0.2.2:8014/firstapp/abc");
//                    String info = "joe";
//                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                    conn.setDoOutput(true);
//                    DataOutputStream dataOutputStream = new DataOutputStream(conn.getOutputStream());
//                    dataOutputStream.writeBytes(info);
//                    dataOutputStream.flush();
//                    dataOutputStream.close();
//                    int responseCode =  conn.getResponseCode();
//                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//                    String line ="";
//                    StringBuilder responseOutput =  new StringBuilder();
//                    while((line=br.readLine())!=null)
//                    {
//                        responseOutput.append(line);
//                    }
//                    br.close();
//                    String output = responseOutput.toString();
//                    content.setText(output);
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                JSONParser mJSONParser = new JSONParser();
                mJSONParser.execute();
//                String c =username.getText().toString();
//                String d =password.getText().toString();
//                verify q = new verify(c,d);
//                q.execute();

            }
        });


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.


    }
//
//   public class verify extends AsyncTask<String, Void, String>
//   {
//        OutputStream os =null;
//       String url = "http://10.0.2.2:8014/firstapp/student_check/";
//       String a,b;
//       public verify(String c, String d)
//       {
//            a=c;
//           b=d;
//           System.out.println(a);
//           System.out.println(b);
//       }
//       String f="";
//       @Override
//       protected String doInBackground(String... params)
//       {
//           try{
//
//               DefaultHttpClient httpClient = new DefaultHttpClient();
//               HttpPost httpPost = new HttpPost(url);
//               List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
//               nameValuePairs.add(new BasicNameValuePair("username", a));
//               nameValuePairs.add(new BasicNameValuePair("password", b));
//               httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//               HttpResponse response = httpClient.execute(httpPost);
//               BufferedReader rd = new BufferedReader(new InputStreamReader(
//                       response.getEntity().getContent()));
//               String line = "";
//
//               while ((line = rd.readLine()) != null) {
//                   f=f+line;
//               }
//
//
//
//
//
//
//           }
//           catch (Exception e)
//           {
//               e.printStackTrace();
//           }
//
//        return f;
//       }
//       protected void onPostExecute(String page)
//        {
//            String red="1";
//            System.out.print(f);
//
//            if(f.equals(red))
//            {
//                System.out.print(f);
//
//                Intent intent = new Intent(MainActivity.this,Main2Activity.class);
//                intent.putExtra("username",a);
//                startActivity(intent);
//
//            }
//            else {
//
//            }
//
//       }
//   }


    public class JSONParser extends AsyncTask<String, Void, String> {

        InputStream is = null;
        String url = "http://10.0.2.2:8014/firstapp/abc";
        JSONObject jObj = null;
        String json = "";

        // constructor
        public JSONParser() {

        }

        @Override
        protected String doInBackground(String... params) {


            // Making HTTP request
            try {
                // defaultHttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpPost = new HttpGet(url);

                HttpResponse getResponse = httpClient.execute(httpPost);
                final int statusCode = getResponse.getStatusLine().getStatusCode();

                if (statusCode != HttpStatus.SC_OK) {
                    Log.w(getClass().getSimpleName(),
                            "Error " + statusCode + " for URL " + url);
                    return null;
                }

                HttpEntity getResponseEntity = getResponse.getEntity();

                //HttpResponse httpResponse = httpClient.execute(httpPost);
                //HttpEntity httpEntity = httpResponse.getEntity();
                is = getResponseEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                Log.d("IO", e.getMessage().toString());
                e.printStackTrace();

            }

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                json = sb.toString();
            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }

            // try parse the string to a JSON object
            try {
                jObj = new JSONObject(json);
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }

            // return JSON String
            return json;


        }

        protected void onPostExecute(String page) {
            final Context context = getApplicationContext();
            final int duration = Toast.LENGTH_LONG;
            Toast t= Toast.makeText(context,json,duration);
            t.show();


        }
    }


}




































